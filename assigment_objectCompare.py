from object import a,b

# Method Name : compareBtDictionary
# This method using for Comapre between two dictionary
def compareBtDictionaryWithInstance(first_dictionary,second_dictionary):
    diffList = []
    if first_dictionary!=second_dictionary:
        for key in first_dictionary:
            if key not in second_dictionary:
                diffList.append("key: "+ key +" is not present into second Object")
            elif key in second_dictionary and first_dictionary[key] != second_dictionary[key] and type(first_dictionary[key]) != type(second_dictionary[key]):
                diffList.append("key: "+ key + " have different value in both Objects")
            elif key in second_dictionary and type(first_dictionary[key]) == type(second_dictionary[key]) and first_dictionary[key] != second_dictionary[key]:
                tempObjectfirst = first_dictionary[key]
                tempObjectSecond = second_dictionary[key]
                if isinstance(tempObjectfirst,dict)  :
                    diffList.extend(compareBtDictionaryWithInstance(tempObjectfirst,tempObjectSecond))
                elif isinstance(tempObjectfirst,list) and len(tempObjectfirst) == len(tempObjectSecond):
                    uncommonObjectfirst = []
                    for item in tempObjectfirst:
                        if isinstance(item,dict) and item not in tempObjectSecond:
                            uncommonObjectfirst.append(item)
                        elif isinstance(item,dict) and item in tempObjectSecond:
                            tempObjectSecond.remove(item)
                        else:
                            diffList.append("key: "+ key + " have different value in both Objects")
                            break
                    
                    for index in range(len(uncommonObjectfirst)):
                        diffList.extend(compareBtDictionaryWithInstance(uncommonObjectfirst[index],tempObjectSecond[index]))
                else:
                    diffList.append("key: "+ key + " have different value in both Objects")
        
        for x in list(set(second_dictionary.keys()) - set(first_dictionary.keys())):
            diffList.append("key: "+ x +" is not present into first Object")

    return diffList


def compareBtObjects(first_dictionary,second_dictionary):
    outputList = compareBtDictionaryWithInstance(first_dictionary,second_dictionary)
    if len(outputList) == 0:
        return "Both Objects are same"
    else:
        return outputList


if __name__== "__main__":
    finalOutput = compareBtObjects(a,b)
    print(finalOutput)
                       
